#!/bin/sh
set -o errexit
set -o nounset

DebFileBaseName="xkb-patch-layout-n900"
fakeroot -- dpkg-deb \
    -Zbzip2 -z9 \
    --new \
    --build package "$DebFileBaseName.deb"

Ver=`dpkg-deb --field "$DebFileBaseName.deb" Version`
Arc=`dpkg-deb --field "$DebFileBaseName.deb" Architecture`
mv "$DebFileBaseName.deb" "${DebFileBaseName}_${Ver}_${Arc}.deb"

