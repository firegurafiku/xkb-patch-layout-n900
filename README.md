Custom keyboard layouts for Nokia N900
======================================
**Authors:** [Pavel Kretov](mailto:firegurafiku@gmail.com),
            [Arkady Glazov](mailto:uglobster@gmail.com)  
**License:** [GNU GPL of any version](<http://en.wikipedia.org/wiki/GNU_GPL>)  
**Language:** Shell

This package helps setting up localized keyboard layout for Nokia N900 devices
with different key labeling. There is currently support only for Russian, though.
You may need installing this package if you have bought a Nokia N900 phone
with US, German or Finnish/Sweden hardware keyboard, but still want to type
Russian texts in it. See project wiki for more information.