-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 1.0
Source: xkblayouts-rx51-ru
Version: 0.7.0-1
Binary: xkblayouts-rx51-ru
Maintainer: Glazov Arkady <uglobster@gmail.com>
Architecture: any
Standards-Version: 3.7.2
Build-Depends: debhelper (>= 5)
Bugtracker: mailto:uglobster@gmail.com
Files: 
 d17a134fea81600f2746426b5f427c6b 8666 xkblayouts-rx51-ru_0.7.0-1.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.9 (GNU/Linux)

iEYEARECAAYFAkuzpL0ACgkQcVlK3LEf/y/GfwCgrFXNUIbzlK0fhWJbKSnMZqdV
30IAoIubaNhfgbtfOdCBtyf2KhLB0MKD
=tfcc
-----END PGP SIGNATURE-----
